<?php
Breadcrumbs::for('admin.modules.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Module Management', route('admin.modules.index'));
});

Breadcrumbs::for('admin.modules.create', function ($trail) {
    $trail->parent('admin.modules.index');
    $trail->push(__('labels.backend.modules.create'), route('admin.modules.create'));
});

Breadcrumbs::for('admin.modules.show', function ($trail, $id) {
    $trail->parent('admin.modules.index');
    $trail->push(__('menus.backend.modules.view'), route('admin.modules.show', $id));
});

Breadcrumbs::for('admin.modules.edit', function ($trail, $id) {
    $trail->parent('admin.modules.index');
    $trail->push(__('menus.backend.modules.edit'), route('admin.modules.edit', $id));
});