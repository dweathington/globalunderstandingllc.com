<?php
Breadcrumbs::for('admin.students.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Student Management', route('admin.students.index'));
});

Breadcrumbs::for('admin.students.create', function ($trail) {
    $trail->parent('admin.students.index');
    $trail->push(__('labels.backend.students.create'), route('admin.students.create'));
});

Breadcrumbs::for('admin.students.show', function ($trail, $id) {
    $trail->parent('admin.students.index');
    $trail->push(__('menus.backend.students.view'), route('admin.students.show', $id));
});

Breadcrumbs::for('admin.students.edit', function ($trail, $id) {
    $trail->parent('admin.students.index');
    $trail->push(__('menus.backend.students.edit'), route('admin.students.edit', $id));
});