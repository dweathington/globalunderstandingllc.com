<?php
Breadcrumbs::for('admin.organizations.index', function ($trail) {
$trail->parent('admin.dashboard');
$trail->push('Organization Management', route('admin.organizations.index'));
});

Breadcrumbs::for('admin.organizations.create', function ($trail) {
$trail->parent('admin.organizations.index');
$trail->push(__('labels.backend.organizations.create'), route('admin.organizations.create'));
});

Breadcrumbs::for('admin.organizations.show', function ($trail, $id) {
$trail->parent('admin.organizations.index');
$trail->push(__('menus.backend.organizations.view'), route('admin.organizations.show', $id));
});

Breadcrumbs::for('admin.organizations.edit', function ($trail, $id) {
$trail->parent('admin.organizations.index');
$trail->push(__('menus.backend.organizations.edit'), route('admin.organizations.edit', $id));
});