<?php
Breadcrumbs::for('admin.surveys.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Survey Management', route('admin.surveys.index'));
});

Breadcrumbs::for('admin.surveys.create', function ($trail) {
    $trail->parent('admin.surveys.index');
    $trail->push(__('labels.backend.surveys.create'), route('admin.surveys.create'));
});

Breadcrumbs::for('admin.surveys.show', function ($trail, $id) {
    $trail->parent('admin.surveys.index');
    $trail->push(__('menus.backend.surveys.view'), route('admin.surveys.show', $id));
});

Breadcrumbs::for('admin.surveys.edit', function ($trail, $id) {
    $trail->parent('admin.surveys.index');
    $trail->push(__('menus.backend.surveys.edit'), route('admin.surveys.edit', $id));
});