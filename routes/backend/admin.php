<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::get('organizations/type', 'OrganizationsController@organizationTypes');
Route::resource('organizations', 'OrganizationsController');
Route::resource('modules', 'ModulesController');
Route::resource('surveys', 'SurveysController');
Route::resource('students', 'StudentsController');
Route::post('students/{id}/clear', 'StudentsController@clearProgress')->name('students.clear');
Route::get('question/{type}/{id}', 'QuestionsController@questionDeleteStatus');
Route::post('question/{type}/{id}', 'QuestionsController@deleteQuestion');