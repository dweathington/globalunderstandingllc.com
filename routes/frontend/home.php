<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'HomeController@index')->name('index');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth']], function () {
    Route::group(['as' => 'user.'], function () {
        Route::get('profile', 'UserController@profile')->name('profile');
    });
    Route::get('survey', 'SurveyController@survey')->name('survey');
    Route::get('module/{module}', 'ModuleController@module')->name('module');

    Route::put('profile/{user}', 'UserController@saveProfile');
    Route::put('survey/{survey}', 'SurveyController@saveResponses');
    Route::put('module/{module}', 'ModuleController@saveResponses');
    Route::get('module/{module}/watched', 'ModuleController@goToPost')->name('module.watched');
});
