@extends ('backend.layouts.app')

@section ('title', __('labels.backend.surveys.management') . ' | ' . __('labels.backend.surveys.edit'))

@section('breadcrumb-links')
    @include('backend.surveys.includes.breadcrumb-links')
@endsection

@section('content')
    <survey-form :survey-form-data="{{ $survey->toJson() }}"></survey-form>
@endsection
