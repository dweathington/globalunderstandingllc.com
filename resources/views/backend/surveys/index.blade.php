@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.surveys.management'))

@section('breadcrumb-links')
    @include('backend.surveys.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.surveys.management') }}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.surveys.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.surveys.table.name') }}</th>
                            <th>{{ __('labels.backend.surveys.table.description') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($surveys as $survey)
                            <tr>
                                <td>{{ $survey->name }}</td>
                                <td>{{ $survey->description }}</td>
                                <td>{!! $survey->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $surveys->total() !!} {{ trans_choice('labels.backend.surveys.table.total', $surveys->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $surveys->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
