@extends ('backend.layouts.app')

@section ('title', __('labels.backend.organizations.management') . ' | ' . __('labels.backend.organizations.view'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.surveys.management') }}
                        <small class="text-muted">{{ __('labels.backend.surveys.view') }}</small>
                    </h4>
                </div><!--col-->
                <div class="col ml-auto text-right">
                    <a href="{{route('admin.surveys.edit', $survey)}}" data-toggle="tooltip" data-placement="top" title="{{__('buttons.general.crud.edit')}}" class="btn btn-primary">Edit</a>
                </div>
            </div><!--row-->
            <div class="row mt-4">
                <div class="col">
                    <h2>{{ $survey->name }}</h2>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-8">
                    <p>{{ $survey->description }}</p>
                </div>
            </div>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#pre" role="tab" aria-controls="pre" aria-expanded="true">Survey Questions</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="pre" role="tabpanel" aria-expanded="true">
                            @foreach($survey->surveyQuestions as $question )
                                <div>
                                    <div class="text-left">
                                        <strong>Question #{{ $question->question_number }}</strong>
                                    </div>
                                    <h3 class="text-left mt-2 mb-4">{{ $question->question_text}}</h3>
                                </div>
                                @switch($question->question_type)
                                    @case('multiple_choice')
                                    <div class="mb-4">
                                        <div class="margin-center">
                                            <div class="row">
                                                @foreach($question->question_options as $key => $option)
                                                    <div class="col-sm-6 col-12 col-xl-3 text-left" >
                                                        <label class="radio-label row align-items-baseline">
                                                            <div class="col-auto">
                                                                <input
                                                                        type="radio"
                                                                        class="option-input radio"
                                                                />
                                                            </div>
                                                            <div class="col">{{ $option }}</div>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @break
                                    @case('short_answer')
                                    <div class="mb-4">
                                        <div class="form-group">
                                            <input
                                                    type="text"
                                                    placeholder="Type your Answer"
                                                    class="form-control"
                                            >
                                        </div>
                                    </div>
                                    @break
                                    @case('paragraph')
                                    <div class="mb-4">
                                        <div class="form-group">
                                      <textarea
                                              placeholder="Type your Answer"
                                              rows="5"
                                              class="form-control"
                                      ></textarea>
                                        </div>
                                    </div>
                                    @break
                                    @case('true_false')
                                    <div class="mb-4">
                                        <label class="radio-label mr-2 clickable">
                                            <input
                                                    type="radio"
                                                    class="option-input radio"
                                                    value="1"
                                            />
                                            True
                                        </label>
                                        <label class="radio-label ml-2 clickable">
                                            <input
                                                    type="radio"
                                                    class="option-input radio"
                                                    value="0"
                                            />
                                            False
                                        </label>
                                    </div>
                                    @break
                                @endswitch
                            @endforeach
                        </div><!--tab-->
                    </div><!--tab-content-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <small class="float-right text-muted">

                    </small>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
@endsection
