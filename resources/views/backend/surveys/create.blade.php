@extends ('backend.layouts.app')

@section ('title', __('labels.backend.surveys.management') . ' | ' . __('labels.backend.surveys.create'))

@section('breadcrumb-links')
    @include('backend.surveys.includes.breadcrumb-links')
@endsection

@section('content')
    <survey-form></survey-form>
@endsection