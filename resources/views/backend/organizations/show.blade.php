@extends ('backend.layouts.app')

@section ('title', __('labels.backend.organizations.management') . ' | ' . __('labels.backend.organizations.view'))

@section('breadcrumb-links')

@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ $organization->name }}
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="mt-4 mb-4">
            <div class="row mb-2">
                <div class="col-2">
                    <strong>Type</strong>
                </div><!--col-->
                <div class="col-10">
                    {{ $organization->type->name }}
                </div><!--col-->
            </div><!--row-->
            <div class="row mb-2">
                <div class="col-2">
                    <strong>Address</strong>
                </div>
                <div class="col-10">
                    {{ $organization->address_1 }}<br>
                    {{ $organization->address_2 }}<br>
                    {{ $organization->city }}, {{ $organization->state }} {{$organization->zip}}
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-2">
                    <strong>Phone</strong>
                </div>
                <div class="col-10">
                    {{ $organization->phone }}
                </div>
            </div>

            <div class="row mb-2">
                <div class="col text-truncate" style="max-width: {{ 100 / $modules->count() + 1 }}%">
                    <strong>Registered users</strong>
                </div>
                @foreach($modules as $module)
                    <div class="col text-truncate" style="max-width: {{ 100 / $modules->count() + 1 }}%">
                        <strong>{{ $module->name }}</strong>
                    </div>
                @endforeach
            </div>
            <div class="row">
                <div class="col text-center">
                    {{ $organization->users->count() }}
                </div>
                @foreach($modules as $module)
                    <div class="col text-center">
                        {{ $organization->students()
                            ->where(['module_id' => $module->id, 'module_status' => 'Completed'])
                             ->count()
                         }} / {{ $organization->users->count() }}
                    </div>
                @endforeach
            </div>
        </div>


    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted text-right">
                    <strong>Created At:</strong> {{ timezone()->convertToLocal($organization->created_at) }} ({{ $organization->created_at->diffForHumans() }})<br>
                    <strong>Last Updated:</strong> {{ timezone()->convertToLocal($organization->created_at) }} ({{ $organization->created_at->diffForHumans() }})

                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
