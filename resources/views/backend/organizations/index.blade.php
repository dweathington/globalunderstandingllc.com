@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.organizations.management'))

@section('breadcrumb-links')
    @include('backend.organizations.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.organizations.management') }}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.organizations.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.organizations.table.name') }}</th>
                            <th>{{ __('labels.backend.organizations.table.type') }}</th>
                            <th>{{ __('labels.backend.organizations.table.city') }}</th>
                            <th>{{ __('labels.backend.organizations.table.state') }}</th>
                            <th>{{ __('labels.backend.organizations.table.phone') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($organizations as $organization)
                            <tr>
                                <td>{{ $organization->name }}</td>
                                <td>{{ $organization->type ? $organization->type->name : '' }}</td>
                                <td>{!! $organization->city !!}</td>
                                <td>{!! $organization->state !!}</td>
                                <td>{{ $organization->phone }}</td>
                                <td>{!! $organization->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $organizations->total() !!} {{ trans_choice('labels.backend.organizations.table.total', $organizations->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $organizations->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
