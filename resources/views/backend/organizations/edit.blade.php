@extends ('backend.layouts.app')

@section ('title', __('labels.backend.organizations.management') . ' | ' . __('labels.backend.organizations.edit'))

@section('breadcrumb-links')
    @include('backend.organizations.includes.breadcrumb-links')
@endsection

@section('content')
    <organization-form :organization-form-data="{{ $organization->toJson() }}" :organization-types="{{ $organizationTypes }}"></organization-form>
@endsection
