@extends ('backend.layouts.app')

@section ('title', __('labels.backend.organizations.management') . ' | ' . __('labels.backend.organizations.create'))

@section('breadcrumb-links')
    @include('backend.organizations.includes.breadcrumb-links')
@endsection

@section('content')
    <organization-form :organization-types="{{ $organizationTypes }}"></organization-form>
@endsection