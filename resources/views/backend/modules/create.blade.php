@extends ('backend.layouts.app')

@section ('title', __('labels.backend.modules.management') . ' | ' . __('labels.backend.modules.create'))

@section('breadcrumb-links')
    @include('backend.modules.includes.breadcrumb-links')
@endsection

@section('content')
    <module-form></module-form>
@endsection