@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.modules.management'))

@section('breadcrumb-links')
    @include('backend.modules.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.modules.management') }}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.modules.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.modules.table.name') }}</th>
                            <th>{{ __('labels.backend.modules.table.description') }}</th>
                            <th>{{ __('labels.backend.modules.table.instructor') }}</th>
                            <th>{{ __('labels.backend.modules.table.video') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($modules as $module)
                            <tr>
                                <td>{{ $module->name }}</td>
                                <td>{{ $module->description }}</td>
                                <td>{{ $module->instructor }}</td>
                                <td>{!! $module->video_url !!}</td>
                                <td>{!! $module->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $modules->total() !!} {{ trans_choice('labels.backend.modules.table.total', $modules->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $modules->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
