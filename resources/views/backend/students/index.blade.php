@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('labels.backend.students.management'))

@section('breadcrumb-links')
    @include('backend.students.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('labels.backend.students.management') }}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.students.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>{{ __('labels.backend.students.table.name') }}</th>
                            <th>{{ __('labels.backend.students.table.organization') }}</th>
                            <th>{{ __('labels.backend.students.table.modules') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($students as $user)
                            <tr>
                                <td>{{ $user->first_name }} {{ $user->last_name }}</td>
                                <td>{{ $user->organization ? $user->organization->name : '' }}</td>
                                <td>
                                    <table class="table-sm table-clear">
                                        <thead>
                                        <tr>
                                            <th>Module Name</th>
                                            <th>Status</th>
                                        </tr>
                                        </thead>
                                        @foreach($user->students as $student)
                                            <tr>
                                                <td>{{ $student->module->name }}</td><td>{{ $student->module_status }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                                <td>
                                    <div class="btn-group" role="group" aria-label="Organization Actions">
                                        <a
                                            href="{{ route('admin.students.show', $user)  }}"
                                            data-toggle="tooltip" data-placement="top"
                                            title="{{ __('buttons.general.crud.view') }}"
                                            class="btn btn-info"
                                        >
                                            <i class="fas fa-eye text-white"></i>
                                        </a>
                                        <form
                                            method="POST"
                                            action="{{route('admin.students.clear', ['id' => $student->user_id])}}"
                                            onsubmit="return confirm('Are you sure you want to reset this user\'s progress? This action cannot be undone.');"
                                        >
                                            @csrf
                                            <button class="btn btn-danger" type="submit">
                                                <i class="fas fa-trash-alt text-white"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $students->total() !!} {{ trans_choice('labels.backend.students.table.total', $students->total()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $students->render() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
