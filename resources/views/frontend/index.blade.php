@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.general.home'))

@section('content')
    <div class="row mb-4 main-container p-4">
        <div class="col">
            <div class="text-center">
                <img
                        class="img-fluid"
                        src="/images/logo.png?102fa857394e0a879e0d19a4c5876351"
                        style="width:122px; height:auto;"
                />
                <h2 class="mt-4">{{ __('strings.frontend.welcome_to', ['place' => app_name()]) }}</h2>
                @guest
                    <div class="container mt-5" style="max-width:300px;">
                        <div class="row p-2">
                            <div class="col">
                                <a href="{{route('frontend.auth.login')}}" class="btn btn-primary btn-block">
                                    Sign In
                                </a>
                            </div>
                        </div>
                        <div class="row p-2">
                            <div class="col">
                                <a href="{{route('frontend.auth.register')}}" class="btn btn-primary btn-block">
                                    Register
                                </a>
                            </div>
                        </div>
                    </div>
                @else
                    <?php $isProfileComplete = Auth::user()->isProfileComplete(); ?>
                    <?php $isSurveyComplete = Auth::user()->isSurveyComplete(); ?>
                    @if(!$isProfileComplete || !$isSurveyComplete)
                        <div class="container mt-3" style="max-width:300px;">
                            @if(!$isProfileComplete)
                                <div class="row p-2">
                                    <div class="col">
                                        <a
                                                href="{{route('frontend.user.profile')}}"
                                                class="btn btn-outline-primary btn-lg btn-wide"
                                        >
                                            Complete profile
                                        </a>
                                    </div>
                                </div>
                            @endif
                            @if(!$isSurveyComplete)
                                <div class="row p-2">
                                    <div class="col">
                                        <a
                                                href="{{route('frontend.survey')}}"
                                                class="btn btn-outline-primary btn-lg btn-wide"
                                        >
                                            Complete survey
                                        </a>
                                    </div>
                                </div>
                            @endif

                        </div>
                    @endif
                    @if(Auth::user()->isProfileComplete() && Auth::user()->isSurveyComplete())
                        <div class="container mt-3">
                            <?php $availableModules = Auth::user()->availableModules(); ?>
                            @if($availableModules && count($availableModules) > 0)
                                @foreach($availableModules as $module)
                                    <div class="row p-2">
                                        <div class="col">
                                            <a
                                                    href="{{route('frontend.module', ['module' => $module])}}"
                                                    class="btn btn-outline-primary btn-lg btn-wide module-button"
                                            >
                                                {{ $module->name }}
                                            </a>
                                        </div>
                                    </div>
                                @endforeach

                            @else
                                    <h3 class="text-center mt-5">All modules have been completed!</h3>
                            @endif

                        </div>
                    @else
                        <h3 class="text-center">No currently available modules.</h3>
                    @endif
                @endguest
            </div>
        </div><!--col-->
    </div><!--row-->
@endsection