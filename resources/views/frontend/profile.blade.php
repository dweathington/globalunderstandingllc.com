@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.profile'))

@section('content')
    <div class="row mb-4 main-container p-4">
        <div class="col">
            <h2 class="text-center">{{ __('navs.frontend.profile') }}</h2>
            <div>
                <profile-form
                    :profile-form-data="{{ json_encode($user) }}"
                    :organizations="{{ json_encode($organizations) }}"
                ></profile-form>
            </div>
        </div><!--col-->
    </div><!--row-->
@endsection