@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.survey'))

@section('content')
    <div class="row mb-4 main-container p-4">
        <div class="col">
            <h2 class="text-center">{{ $survey->name  }}</h2>
            <div>
                <p class="mb-0">
                    {{ $survey->description }}
                </p>
                <survey :survey-questions="{{ $survey->surveyQuestions }}" :survey-id="{{ $survey->id }}"></survey>
            </div>
        </div><!--col-->
    </div><!--row-->
@endsection