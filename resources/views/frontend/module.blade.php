@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('navs.frontend.module'))

@section('content')
    <div class="row mb-4 main-container p-4">
        <div class="col">
            <h1 class="text-center">{{ $module->name  }}</h1>
            <div>
                <p class="mb-0">
                    {{ $module->description }}
                </p>
                @switch($student->module_status)
                    @case('pre')
                        <h2 class="text-center mt-3">Pre Module Questions</h2>
                        <module
                            :module-questions="{{ $questions }}"
                            :module-id="{{ $module->id }}"
                            pre-or-post="pre"
                        ></module>
                    @break
                    @case('lesson')
                        <div class="embed-responsive embed-responsive-16by9 mt-4 mb-4">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$videoId}}?rel=0" allowfullscreen></iframe>
                        </div>
                        <div class="row p-2">
                            <div class="col text-center">
                                <a
                                    href="{{route('frontend.module.watched', ['module' => $module])}}"
                                    class="btn btn-success btn-lg btn-wide"
                                >
                                    Next
                                </a>
                            </div>
                        </div>
                    @break
                    @case('post')
                        <h2 class="text-center mt-3">Post Module Questions</h2>
                        <module
                                :module-questions="{{ $questions }}"
                                :module-id="{{ $module->id }}"
                                pre-or-post="post"
                        ></module>
                    @break
                @endswitch
            </div>
        </div><!--col-->
    </div><!--row-->
@endsection