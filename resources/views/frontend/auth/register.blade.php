@extends('frontend.layouts.app')

@section('title', app_name() . ' | '.__('labels.frontend.auth.register_box_title'))

@section('content')
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-6 align-self-center main-container p-4">
            <h2 class="text-center">
                {{ __('labels.frontend.auth.register_box_title') }}
            </h2>
            <div>
                {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.first_name'))
                                ->attribute('maxlength', 191) }}
                        </div><!--col-->
                    </div><!--row-->

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

                            {{ html()->text('last_name')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.last_name'))
                                ->attribute('maxlength', 191) }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.organization'))->for('organization_id') }}

                            {{ html()->select('organization_id', $organizations)
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.organization_placeholder'))
                                ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.email'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                            {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.password'))
                                ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                            {{ html()->password('password_confirmation')
                                ->class('form-control')
                                ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.age_range'))->for('age_range') }}
                            {{ html()->select('age_range')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    '18-24 years old' => '18-24 years old',
                                    '25-34 years old' => '25-34 years old',
                                    '35-44 years old' => '35-44 years old',
                                    '45-54 years old' => '45-54 years old',
                                    'Over 55' => 'Over 55',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.gender'))->for('gender') }}
                            {{ html()->select('gender')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    'Male' => 'Male',
                                    'Female' => 'Female',
                                    'Other' => 'Other',
                                    'Prefer not to say' => 'Prefer not to say',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.ethnicity'))->for('ethnicity') }}
                            {{ html()->select('ethnicity')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    'White' => 'White',
                                    'Hispanic or Latino' => 'Hispanic or Latino',
                                    'Black or African American' => 'Black or African American',
                                    'Native American' => 'Native American',
                                    'Asian/Pacific Islander' => 'Asian/Pacific Islander',
                                    'Other' => 'Other',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.education'))->for('education') }}
                            {{ html()->select('education')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    'High school degree or equivalent' => 'High school degree or equivalent',
                                    'Bachelor\'s degree (e.g. BA, BS)' => 'Bachelor\'s degree (e.g. BA, BS)',
                                    'Master\'s degree (e.g. MA, MS, MEd)' => 'Master\'s degree (e.g. MA, MS, MEd)',
                                    'Doctorate (e.g. PhD, EdD)' => 'Doctorate (e.g. PhD, EdD)',
                                    'Other' => 'Other',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.marital_status'))->for('marital_status') }}
                            {{ html()->select('marital_status')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    'Single (never married)' => 'Single (never married)',
                                    'Married' => 'Married',
                                    'In a domestic partnership' => 'In a domestic partnership',
                                    'Divorced' => 'Divorced',
                                    'Widowed' => 'Widowed',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->label(__('validation.attributes.frontend.household_income'))->for('household_income') }}
                            {{ html()->select('household_income')
                                ->class('form-control')
                                ->placeholder('Select one')
                                ->options([
                                    'Below $10k' => 'Below $10k',
                                    '$10k - $50k' => '$10k - $50k',
                                    '$50k - $100k' => '$50k - $100k',
                                    '$100k - $150k' => '$100k - $150k',
                                    'Over $150k' => 'Over $150k',
                                ]) }}
                        </div>
                    </div><!--col-->
                </div><!--row-->

                @if (config('access.captcha.registration'))
                    <div class="row">
                        <div class="col">
                            {!! Captcha::display() !!}
                            {{ html()->hidden('captcha_status', 'true') }}
                        </div><!--col-->
                    </div><!--row-->
                @endif

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0 clearfix">
                            {{ form_submit(__('labels.frontend.auth.register_button')) }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                {{ html()->form()->close() }}

            </div>
        </div><!-- col-md-8 -->
    </div><!-- row -->
@endsection

@push('after-scripts')
    @if (config('access.captcha.registration'))
        {!! Captcha::script() !!}
    @endif
@endpush
