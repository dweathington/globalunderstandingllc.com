
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
require('../plugins');

window.Vue = require('vue');
import VeeValidate from 'vee-validate';
import Cleave from 'vue-cleave-component';
import 'cleave.js/dist/addons/cleave-phone.us';
import vSelect from 'vue-select';
import ProfileForm from './components/ProfileForm';
import Survey from './components/Survey';
import Module from './components/Module';

Vue.component('v-select', vSelect);
Vue.component('profile-form', ProfileForm);
Vue.component('survey', Survey);
Vue.component('module', Module);
Vue.use(VeeValidate);
Vue.use(Cleave);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
