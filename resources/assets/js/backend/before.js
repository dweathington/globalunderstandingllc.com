/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Loaded before CoreUI app.js
require('../bootstrap');
require('pace');
require('../plugins');
import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Cleave from 'vue-cleave-component';
import 'cleave.js/dist/addons/cleave-phone.us';

window.Vue = require('vue');

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import vSelect from 'vue-select';
import OrganizationForm from './components/Forms/OrganizationForm';
import ModuleForm from './components/Forms/ModuleForm';
import SurveyForm from './components/Forms/SurveyForm';

Vue.component('v-select', vSelect);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('questions', require('./components/Questions.vue'));
Vue.component('organization-form', OrganizationForm);
Vue.component('module-form', ModuleForm);
Vue.component('survey-form', SurveyForm);
Vue.use(VeeValidate);
Vue.use(Cleave);


const app = new Vue({
  el: '#app'
});
