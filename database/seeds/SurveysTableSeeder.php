<?php

use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class SurveysTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        foreach ($this->surveys as $survey) {
            $survey = \App\Models\Survey::create($survey);
            foreach ($this->surveyQuestions as $question) {
                $question['survey_id'] = $survey->id;
                \App\Models\SurveyQuestion::create($question);
            }
        }

        $this->enableForeignKeys();
    }

    private $surveys = array (
        0 =>
            array (
                'id' => 1,
                'name' => 'Intake Survey',
                'description' => 'This survey will give us a feel for what your experience is and how we can best serve your needs.',
                'created_at' => null,
                'updated_at' => '2018-08-30 04:36:51',
                'active' => 1,
            ),
    );
    
    private $surveyQuestions = array (
        0 =>
            array (
                'survey_id' => 1,
                'question_number' => 1,
                'question_text' => 'What is your position title?',
                'question_type' => 'short_answer',
                'question_options' =>
                    array (
                    ),
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:36:51',
            ),
        1 =>
            array (
                'survey_id' => 1,
                'question_number' => 3,
                'question_text' => 'What was the most challenging experience of your career?',
                'question_type' => 'paragraph',
                'question_options' =>
                    array (
                    ),
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:36:51',
            ),
        2 =>
            array (
                'survey_id' => 1,
                'question_number' => 4,
                'question_text' => 'Is your department open and receptive to officer concerns?',
                'question_type' => 'true_false',
                'question_options' =>
                    array (
                    ),
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:36:51',
            ),
        3 =>
            array (
                'survey_id' => 1,
                'question_number' => 5,
                'question_text' => 'How satisfied are you with your precinct?',
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'Very Satisfied',
                        'option_1' => 'Somewhat Satisfied',
                        'option_2' => 'Somewhat Unsatisfied',
                        'option_3' => 'Very Unsatisified',
                    ),
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:36:51',
            ),
        4 =>
            array (
                'survey_id' => 1,
                'question_number' => 2,
                'question_text' => 'How long have you been on the force?',
                'question_type' => 'short_answer',
                'question_options' =>
                    array (
                    ),
                'created_at' => '2018-08-30 04:36:51',
                'updated_at' => '2018-08-30 04:36:51',
            ),
    );
}
