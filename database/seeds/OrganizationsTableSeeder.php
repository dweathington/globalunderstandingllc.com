<?php

use Illuminate\Database\Seeder;

/**
 * Class OrganizationsTableSeeder.
 */
class OrganizationsTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        $typeData = [
            ['name' => 'Police Department'],
            ['name' => 'Law Enforcement Association'],
            ['name' => 'Sheriffs Department'],
            ['name' => 'Campus Police Department'],
            ['name' => 'Adult Probation & Parole'],
            ['name' => 'Marshals Department'],
        ];

        $data = [
            [
                'name' => 'Langston University Police Department',
                'type_id' => 1,
                'city' => 'Langston',
                'state' => 'OK',
            ],
            [
                'name' => 'Lodi Police Department',
                'type_id' => 1,
                'city' => 'Lodi',
                'state' => 'NJ',
            ],
            [
                'name' => 'Texas Response Group',
                'type_id' => 1,
                'city' => 'Dallas',
                'state' => 'TX',
            ],
            [
                'name' => 'Tyngsborough Police Department',
                'type_id' => 1,
                'city' => 'Tyngsborough',
                'state' => 'MA',
            ],
            [
                'name' => 'Homecroft Police Department',
                'type_id' => 1,
                'city' => 'Indianapolis',
                'state' => 'IN',
            ],
            [
                'name' => 'Bethel Police Department',
                'type_id' => 1,
                'city' => 'Bethel',
                'state' => 'OH',
            ],

            [
                'name' => 'New York City Police Department',
                'type_id' => 1,
                'city' => 'New York City',
                'state' => 'NY',
            ],
            [
                'name' => 'Houston Police Department',
                'type_id' => 1,
                'city' => 'Houston',
                'state' => 'TX',
            ],
            [
                'name' => 'Chicago Police Department',
                'type_id' => 1,
                'city' => 'Dallas',
                'state' => 'TX',
            ],
            [
                'name' => 'Benton County Environmental Enforcement',
                'type_id' => 2,
                'city' => 'Bentonville',
                'state' => 'AR',
            ],
            [
                'name' => 'Cullman County Sheriff\'s Office',
                'type_id' => 3,
                'city' => 'Cullman',
                'state' => 'AL',
            ],
            [
                'name' => 'Whitehall Township Police',
                'type_id' => 1,
                'city' => 'Whitehall',
                'state' => 'PA',
            ],

            [
                'name' => 'Jackson County Sheriff\'s Office',
                'type_id' => 3,
                'city' => 'Maquoketa',
                'state' => 'IA',
            ],
            [
                'name' => 'Valdosta State University Police Department',
                'type_id' => 4,
                'city' => 'Valdosta',
                'state' => 'GA',
            ],
            [
                'name' => 'Georgia Department of Community Supervision',
                'type_id' => 5,
                'city' => 'Atlanta',
                'state' => 'GA',
            ],
            [
                'name' => 'Holland Department of Public Safety',
                'type_id' => 1,
                'city' => 'Holland',
                'state' => 'MI',
            ],
            [
                'name' => 'Asbury Park Police Department',
                'type_id' => 1,
                'city' => 'Asbury park',
                'state' => 'NJ',
            ],
            [
                'name' => 'Aurora Marshal\'s Office',
                'type_id' => 6,
                'city' => 'Aurora',
                'state' => 'CO',
            ],

            [
                'name' => 'University of Southern Indiana Public Safety Office',
                'type_id' => 4,
                'city' => 'Evansville',
                'state' => 'IN',
            ],

            [
                'name' => 'Ceredo Police Department',
                'type_id' => 1,
                'city' => 'Ceredo',
                'state' => 'WV',
            ],
            [
                'name' => 'University of Alaska Fairbanks Police Department',
                'type_id' => 1,
                'city' => 'Fairbanks',
                'state' => 'AK',
            ],
            [
                'name' => 'Port of Portland Police Department',
                'type_id' => 1,
                'city' => 'Portland',
                'state' => 'OR',
            ],
            [
                'name' => 'Sidney Municipal Court',
                'type_id' => 5,
                'city' => 'Sidney',
                'state' => 'OH',
            ],

            [
                'name' => 'Double Oak Police Department',
                'type_id' => 1,
                'city' => 'Double Oak',
                'state' => 'TX',
            ],
        ];

        foreach ($typeData as $typeParams) {
            \App\Models\OrganizationType::create($typeParams);
        }

        foreach ($data as $attr) {
            \App\Models\Organization::create($attr);
        }

        $this->enableForeignKeys();
    }
}
