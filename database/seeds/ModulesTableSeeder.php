<?php

use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class ModulesTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        foreach ($this->modules as $module) {
            $module = \App\Models\Module::create($module);
            foreach ($this->moduleQuestions as $question) {
                $question['module_id'] = $module->id;
                \App\Models\ModuleQuestion::create($question);
            }
        }

        $this->enableForeignKeys();
    }

    private $modules = array (
        0 =>
            array (
                'id' => 1,
                'name' => 'Psychology of law enforcement',
                'description' => 'This block will assist law enforcement in balancing a home life, security of one’s self, increasing E.Q. (emotional intelligence), and serving as a community liaison while enforcing the law.',
                'instructor' => null,
                'video_url' => "https://elasticbeanstalk-us-east-2-065360436118.s3.us-east-2.amazonaws.com/resources/video/PsychologyOfLawEnforcement.mov",
            ),
        1 =>
            array (
                'id' => 2,
                'name' => 'Societal norms (Gender equality, Cultural proficiency, and Social awareness)',
                'description' => 'This module will aid officers in understanding and identifying their cultural biases. This cohort will also train law enforcement to be sensitive in their level of enforcement when dealing with women and children. According to the Cato Institute ﻿“more than 9 percent of reports of police misconduct in 2010 involved sexual abuse, making it the second-most reported form of misconduct, after the use of excessive force.”(10)',
                'instructor' => null,
                'video_url' => "https://elasticbeanstalk-us-east-2-065360436118.s3.us-east-2.amazonaws.com/resources/video/societalNorms.mp4",
            ),
        2 =>
            array (
                'id' => 3,
                'name' => 'Non-evasive restraint techniques and levels of force',
                'description' => 'These techniques are especially effective when officers find themselves in close quarters and need to control a situation or make an arrest as an alternative to using deadly force. The Five Principles is derived from five fundamental components using concepts from San Da JiuJitsu Do, Aikido, Judo, Jiu-jitsu, and Vee-Jitsu.',
                'instructor' => null,
                'video_url' => "https://elasticbeanstalk-us-east-2-065360436118.s3.us-east-2.amazonaws.com/resources/video/NonEvasiveRestraint.mov",
            ),
        3 =>
            array (
                'id' => 4,
                'name' => 'The Law and ethical decision making',
                'description' => 'Understanding the complexities of law enforcement, experts and community officials believe law enforcement officers should be trained in how to better serve their community while maintaining proper ethics. This should be included in yearly professional development training. Balancing the equation of enforcing the law while being a resource for the community; our module emphasizes accountability and how to better serve the community by using “deadly force” only as a last resort.',
                'instructor' => null,
                'video_url' => "https://elasticbeanstalk-us-east-2-065360436118.s3.us-east-2.amazonaws.com/resources/video/LawAndEthicalDecisionMaking.mp4",
            ),
        4 =>
            array (
                'id' => 5,
                'name' => 'Efficacy In Law Enforcement',
                'description' => 'To reach rigorous standards of efficiency our faculty will provide a supportive foundation for the challenging work of law enforcement. The Five Principles promotes the idea that effective effort drives development. By using data and feedback we will develop a strategy to drive proficiency and efficacy in the field.',
                'instructor' => null,
                'video_url' => "https://elasticbeanstalk-us-east-2-065360436118.s3.us-east-2.amazonaws.com/resources/video/Efficacy_In_Law_Enforcement.mp4",
            ),
    );
    
    private $moduleQuestions = array (
        0 =>
            array (
                'question_number' => 1,
                'question_text' => 'The United States Constitution provides that persons shall not be subjected to unreasonable searches and/or seizures.  According to the preceding sentence',
                'question_answer' => null,
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'The police may not conduct a search of a person\'s home without a valid Search Warrant.',
                        'option_1' => 'The police may not seize a person without establishing probable cause.',
                        'option_2' => 'Non-citizens are not protected by the provisions of the United States Constitution.',
                        'option_3' => 'Certain kinds of searches are forbidden by the United States Constitution.',
                    ),
                'pre_or_post' => 'pre',
                'module_id' => 1,
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 05:19:08',
            ),
        1 =>
            array (
                'question_number' => 2,
                'question_text' => 'According to William K. Muir Jr., in order for officers to operate as true professionals they must possess...',
                'question_answer' => null,
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'Passion and desire',
                        'option_1' => 'Will and perspective',
                        'option_2' => 'Passion and perspective',
                        'option_3' => 'Education and morals',
                    ),
                'pre_or_post' => 'pre',
                'module_id' => 1,
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 05:19:08',
            ),
        2 =>
            array (
                'question_number' => 3,
                'question_text' => 'Character refers to a person\'s "outer" makeup while personality refers to their "inner" makeup?',
                'question_answer' => null,
                'question_type' => 'true_false',
                'question_options' =>
                    array (
                    ),
                'pre_or_post' => 'pre',
                'module_id' => 1,
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        3 =>
            array (
                'question_number' => 4,
                'question_text' => 'Good ethical conduct comes primarily from what a person already has before he/she enrolls in the police academy.',
                'question_answer' => null,
                'question_type' => 'true_false',
                'question_options' =>
                    array (
                        'option_0' => 'Option 1',
                        'option_1' => 'Option 2',
                        'option_2' => 'Option 3',
                        'option_3' => 'Option 4',
                    ),
                'pre_or_post' => 'pre',
                'module_id' => 1,
                'created_at' => '2018-08-28 07:53:41',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        4 =>
            array (
                'question_number' => 5,
                'question_text' => 'Police must apply the law...',
                'question_answer' => null,
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'Fairly',
                        'option_1' => 'Evenhandedly',
                        'option_2' => 'With a view to promote justice',
                        'option_3' => 'All answers correct',
                    ),
                'pre_or_post' => 'pre',
                'module_id' => 1,
                'created_at' => '2018-08-30 04:55:05',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        5 =>
            array (
                'question_number' => 1,
                'question_text' => 'Police work is all about making _________________ about other people\'s lives.',
                'question_answer' => null,
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'Jokes',
                        'option_1' => 'Sense',
                        'option_2' => 'Critical decisions',
                        'option_3' => 'Easy choices',
                    ),
                'pre_or_post' => 'post',
                'module_id' => 1,
                'created_at' => '2018-08-30 04:55:05',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        6 =>
            array (
                'question_number' => 2,
                'question_text' => 'The essential element in police professionalism is...',
                'question_answer' => null,
                'question_type' => 'multiple_choice',
                'question_options' =>
                    array (
                        'option_0' => 'Good character',
                        'option_1' => 'Compassion',
                        'option_2' => 'Willingness to learn',
                        'option_3' => 'Tolerance',
                    ),
                'pre_or_post' => 'post',
                'module_id' => 1,
                'created_at' => '2018-08-30 04:55:05',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        7 =>
            array (
                'question_number' => 3,
                'question_text' => 'The application of the law must be done fairly and in an impartial manner.  This statement implies that officers must not care about people.',
                'question_answer' => null,
                'question_type' => 'true_false',
                'question_options' =>
                    array (
                    ),
                'pre_or_post' => 'post',
                'module_id' => 1,
                'created_at' => '2018-08-30 04:55:05',
                'updated_at' => '2018-08-30 04:55:05',
            ),
        8 =>
            array (
                'question_number' => 4,
                'question_text' => 'Why is policing ethics important?',
                'question_answer' => null,
                'question_type' => 'paragraph',
                'question_options' =>
                    array (
                    ),
                'pre_or_post' => 'post',
                'module_id' => 1,
                'created_at' => '2018-08-30 04:55:05',
                'updated_at' => '2018-08-30 04:55:05',
            ),
    );
}
