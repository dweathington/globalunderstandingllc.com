<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        Admin::find(1)->assignRole(config('access.users.admin_role'));

        $this->enableForeignKeys();
    }
}
