<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('module_question_id');
            $table->unsignedInteger('module_id');
            $table->text('answer');
            $table->boolean('correct')->nullable();
            $table->dateTime('answered_at');
            $table->unsignedInteger('student_id');
            $table->timestamps();
            $table->foreign('module_question_id')->references('id')->on('module_questions');
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_responses');
    }
}
