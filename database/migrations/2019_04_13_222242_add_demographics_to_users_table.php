<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDemographicsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('age_range')->nullable();
            $table->string('gender')->nullable();
            $table->string('ethnicity')->nullable();
            $table->string('education')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('household_income')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('age_range');
            $table->removeColumn('gender');
            $table->removeColumn('ethnicity');
            $table->removeColumn('education');
            $table->removeColumn('marital_status');
            $table->removeColumn('household_income');
        });
    }
}
