<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_number');
            $table->text('question_text');
            $table->text('question_answer')->nullable();
            $table->string('question_type');
            $table->json('question_options')->nullable();
            $table->string('pre_or_post');
            $table->unsignedInteger('module_id');
            $table->timestamps();
            $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_questions');
    }
}
