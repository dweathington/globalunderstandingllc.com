<?php

namespace App\Repositories\Backend;

use App\Models\Organization;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class OrganizationRepository.
 */
class OrganizationRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Organization::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            //->with('roles', 'permissions', 'providers')
            //->active()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

}
