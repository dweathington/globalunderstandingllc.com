<?php

namespace App\Repositories\Backend;

use App\Models\SurveyQuestion;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ModuleQuestionRepository.
 */
class SurveyQuestionRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return SurveyQuestion::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            //->with('roles', 'permissions', 'providers')
            //->active()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

}
