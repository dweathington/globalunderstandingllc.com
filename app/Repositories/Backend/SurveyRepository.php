<?php

namespace App\Repositories\Backend;

use App\Models\Survey;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SurveyRepository.
 */
class SurveyRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Survey::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

}
