<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    protected $fillable = [
        'survey_id',
        'question_number',
        'question_text',
        'question_type',
        'question_options',
    ];

    protected $casts = [
        'question_options' => 'array',
    ];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function studentResponses()
    {
        return $this->hasMany(UserSurveyResponse::class);
    }
}
