<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class UserSurveyResponse extends Model
{
    protected $fillable = [
        'survey_question_id',
        'survey_id',
        'answer',
        'answered_at',
        'user_id',
    ];

    public function surveyQuestion()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function user()
    {
        return $this->belongsTo(Auth\User::class);
    }
}
