<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
      'organization_id',
      'user_id',
      'module_id',
      'date_taken',
      'module_score',
      'module_num_correct',
      'module_num_incorrect',
      'module_status',
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function user()
    {
        return $this->belongsTo(Auth\User::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function responses()
    {
        return $this->hasMany(StudentResponse::class);
    }

    public function delete()
    {
        $this->responses()->delete();
        return parent::delete();
    }
}
