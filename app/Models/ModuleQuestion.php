<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleQuestion extends Model
{
    protected $fillable = [
        'question_number',
        'question_text',
        'question_answer',
        'question_type',
        'question_options',
        'pre_or_post',
        'module_id',
    ];

    protected $casts = [
        'question_options' => 'array',
    ];

    protected static function boot() {
        parent::boot();

        $filterQuestionOptions = function ($module) {
            if ($module->question_type === 'multiple_choice') {
                $module->question_options = array_filter($module->question_options, function ($value) {
                    return $value !== null and $value !== '';
                });
            }
        };

        static::saving($filterQuestionOptions);

        static::updating($filterQuestionOptions);

        static::creating($filterQuestionOptions);
    }

    public function module() {
        return $this->belongsTo(Module::class);
    }

    public function studentResponses()
    {
        return $this->hasMany(StudentResponse::class);
    }

    public function scopePreQuestions($query)
    {
        return $query->where('pre_or_post', 'pre');
    }

    public function scopePostQuestions($query)
    {
        return $query->where('pre_or_post', 'post');
    }
}
