<?php

namespace App\Models\Auth;

use App\Models\Module;
use App\Models\Organization;
use App\Models\Student;
use App\Models\Survey;
use App\Models\Traits\Uuid;
use App\Models\UserSurveyResponse;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Auth\Traits\SendUserPasswordReset;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Auth\Traits\Relationship\UserRelationship;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use HasRoles,
        Notifiable,
        SendUserPasswordReset,
        SoftDeletes,
        UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope,
        Uuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'password_changed_at',
        'active',
        'confirmation_code',
        'confirmed',
        'timezone',
        'last_login_at',
        'last_login_ip',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'phone',
        'organization_id',
        'age_range',
        'gender',
        'ethnicity',
        'education',
        'marital_status',
        'household_income',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['last_login_at', 'deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
    protected $appends = ['full_name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
        'confirmed' => 'boolean',
    ];

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function surveys()
    {
        return $this->belongsToMany(Survey::class)->withTimestamps();
    }

    public function surveyResponses()
    {
        return $this->hasMany(UserSurveyResponse::class);
    }

    public function isProfileComplete()
    {
        return ($this->first_name &&
            $this->last_name &&
            $this->email &&
            $this->address_1 &&
            $this->city &&
            $this->state &&
            $this->zip &&
            $this->phone &&
            $this->organization_id);
    }

    public function isSurveyComplete()
    {
        $surveys = $this->surveys;
        $survey = $surveys->first();
        if (!$survey) {
            return false;
        }

        $totalQuestions = $survey->surveyQuestions()->count();
        $userResponseCount = $this->surveyResponses()->where('survey_id', $survey->id)->count();

        return $userResponseCount >= $totalQuestions;
    }

    public function availableModules()
    {
        $modules = Module::all();
        $completedModules =  $this->students()->where('module_status', 'completed')
            ->get()->pluck('module_id');
        return $modules->whereNotIn('id', $completedModules);
    }
}
