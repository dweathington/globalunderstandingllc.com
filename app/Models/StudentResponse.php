<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentResponse extends Model
{
    protected $fillable = [
        'module_question_id',
        'module_id',
        'answer',
        'correct',
        'answered_at',
        'student_id',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function moduleQuestion()
    {
        return $this->belongsTo(ModuleQuestion::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
