<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Student;
use App\Models\StudentResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    public function module(Module $module)
    {
        $user = Auth::guard('web')->user();
        $student = $user->students()->where('module_id', $module->id)->first();
        if (!$student) {
            $student = Student::create([
                'organization_id' => $user->organization_id,
                'user_id' => $user->id,
                'module_id' => $module->id,
                'module_status' => 'pre',
            ]);
        }

        if ($student->module_status === 'completed') {
            return redirect()->route('frontend.index');
        }

        $questions = null;
        if ($student->module_status === 'pre') {
            $questions = $module->questions()->preQuestions()->orderBy('question_number')->get();
        } else if ($student->module_status === 'post') {
            $questions = $module->questions()->postQuestions()->orderBy('question_number')->get();
        }

        $videoId = null;
        if ($module->video_url) {
            preg_match(
                '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                $module->video_url,
                $match
            );

            $videoId = $match[1];
        }

        return view('frontend.module')
            ->withModule($module)
            ->withStudent($student)
            ->withQuestions($questions)
            ->withVideoId($videoId);
    }

    public function saveResponses(Request $request, Module $module)
    {
        $user = Auth::guard('web')->user();
        $responses = $request->input('responses');
        $preOrPost = $request->input('preOrPost');

        if (count($responses) !== $module->questions()->{"{$preOrPost}Questions"}()->orderBy('question_number')->count()) {
            return response()->json([
                'status' => 'error',
                'message' => 'All questions must be answered',
            ]);
        }

        $student = Student::firstOrCreate(['user_id' => $user->id, 'module_id' => $module->id],
            ['oranization_id' => $user->organization_id, 'module_status' => $preOrPost]);

        foreach ($responses as $response) {
            $responseData = array_merge($response, [
                'student_id' => $student->id,
                'answered_at' => Carbon::now()
            ]);
            $responseRecord = StudentResponse::create($responseData);
        }

        $moduleStatus = 'pre';
        if ($student->module_status === 'pre') {
            $moduleStatus = 'lesson';
        } else if ($student->module_status === 'post') {
            $moduleStatus = 'completed';
        }

        $student->module_status = $moduleStatus;
        $student->date_taken = Carbon::now();
        $student->save();
        return response()->json([
            'status' => 'success',
        ]);
    }

    public function goToPost(Module $module)
    {
        $user = Auth::guard('web')->user();
        $student = Student::firstOrCreate(['user_id' => $user->id, 'module_id' => $module->id],
            ['oranization_id' => $user->organization_id, 'module_status' => 'lesson']);
        $student->module_status = 'post';
        $student->save();

        return redirect()->route('frontend.module', ['module' => $module]);
    }
}
