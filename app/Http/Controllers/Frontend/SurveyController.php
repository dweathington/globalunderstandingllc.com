<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\UserSurveyResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SurveyController extends Controller
{
    public function survey()
    {
        $survey = Survey::with('surveyQuestions')->where('active', true)->first();
        return view('frontend.survey')->withSurvey($survey);
    }

    public function saveResponses(Request $request, Survey $survey)
    {
        $user = Auth::guard('web')->user();
        $responses = $request->input('responses');
        if (count($responses) !== $survey->surveyQuestions()->count()) {
            return response()->json([
                'status' => 'error',
                'message' => 'All questions must be answered',
            ]);
        }

        foreach ($responses as $response) {
            $responseData = array_merge($response, [
                'user_id' => $user->id,
                'answered_at' => Carbon::now()
            ]);
            $responseRecord = UserSurveyResponse::create($responseData);
        }

        $user->surveys()->attach($survey);
        return response()->json([
            'status' => 'success',
        ]);
    }
}