<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Auth\User;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile()
    {
        $user = \Auth::user();
        return view('frontend.profile')->withUser([
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'address_1' => $user->address_1,
            'address_2' => $user->address_2,
            'city' => $user->city,
            'state' => $user->state,
            'zip' => $user->zip,
            'phone' => $user->phone,
            'organization_id' => $user->organization_id
        ])->withOrganizations(Organization::all()->sortBy('name')->map(function ($org) {
            return ['value' => $org->id, 'label' => $org->name];
        })->values());
    }

    public function saveProfile(Request $request, User $user)
    {
        $loggedInUser = Auth::guard('web')->user();
        if ($loggedInUser->id !== $user->id) {
            return response()->json(['status' => 'error', 'message' => 'Trying to update a different user'])->status(401);
        }
        $user->update($request->only([
            'first_name',
            'last_name',
            'email',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'phone',
            'organization_id',
        ]));

        $request->session()->flash('flash_success', 'Profile saved!');

        return response()->json([
            'status' => 'success',
        ]);
    }
}