<?php

namespace App\Http\Controllers\Backend;

use App\Models\Survey;
use App\Models\SurveyQuestion;
use App\Repositories\Backend\SurveyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveysController extends Controller
{
    protected $surveyRepository;

    public function __construct(SurveyRepository $surveyRepository)
    {
        $this->surveyRepository = $surveyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.surveys.index')
            ->withSurveys($this->surveyRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.surveys.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $updateData = $request->only(['name', 'description']);
        $questions = collect($request->input('questions', []))->pluck('question_data');
        $survey = Survey::create($updateData);

        if (count($questions) > 0) {
            foreach($questions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['survey_id'] = $survey->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = SurveyQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        $request->session()->flash('flash_success', __('alerts.backend.surveys.created'));

        return $survey;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.surveys.show')
            ->withSurvey(Survey::with(['surveyQuestions'])->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        $surveyQuestions = $survey->surveyQuestions->sortBy('question_number')->values()
            ->map(function ($question) {
                return [
                    'id' => $question->id,
                    'question_text' => $question->question_text,
                    'question_type' => $question->question_type,
                    'question_options' => $question->question_options,
                ];
            });

        return view('backend.surveys.edit')
            ->withSurvey(collect([
                "id" => $survey->id,
                "name" => $survey->name,
                "description" => $survey->description,
                "questions" => $surveyQuestions,
            ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        $updateData = $request->only(['name', 'description']);
        $questions = collect($request->input('questions', []))->pluck('question_data');
        $saved = $survey->update($updateData);

        if (count($questions) > 0) {
            foreach($questions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['survey_id'] = $survey->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = SurveyQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        $request->session()->flash('flash_success', __('alerts.backend.surveys.updated'));

        return $survey;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->surveyRepository->deleteById($id);

        return redirect()->route('admin.surveys.index')
            ->withFlashSuccess(__('alerts.backend.surveys.deleted'));
    }
}
