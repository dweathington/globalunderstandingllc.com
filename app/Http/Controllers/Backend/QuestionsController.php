<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\Backend\ModuleQuestionRepository;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\SurveyQuestionRepository;

class QuestionsController extends Controller
{
    protected $moduleQuestionRepository;
    protected $surveyQuestionRepository;

    public function __construct(ModuleQuestionRepository $moduleQuestionRepository, SurveyQuestionRepository $surveyQuestionRepository)
    {
        $this->moduleQuestionRepository = $moduleQuestionRepository;
        $this->surveyQuestionRepository = $surveyQuestionRepository;
    }

    public function questionDeleteStatus($type, $id)
    {
        $repo = $type === 'module' ? $this->moduleQuestionRepository : $this->surveyQuestionRepository;
        $question =  $repo->getById($id);

        if (!$question) {
            return response()->json(['status' => 'error', 'message' => 'Couldn\'t find question'])->status(401);
        }

        $hasResponses = $question->studentResponses()->count();

        if ($hasResponses) {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'hasQuestions' => true,
                ]
            ]);
        }
        return response()->json([
            'status' => 'success',
            'data' => [
                'hasQuestions' => false,
            ]
        ]);
    }

    public function deleteQuestion($type, $id)
    {
        $repo = $type === 'module' ? $this->moduleQuestionRepository : $this->surveyQuestionRepository;
        $question =  $repo->getById($id);
        if ($question) {
            $question->studentResponses()->delete();
        }

        $question->delete();
        return response()->json([
            'status' => 'success',
        ]);
    }
}
