<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\StoreOrganizationRequest;
use App\Http\Requests\Backend\UpdateOrganizationRequest;
use App\Models\Module;
use App\Models\Organization;
use App\Models\OrganizationType;
use App\Repositories\Backend\OrganizationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationsController extends Controller
{
    protected $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.organizations.index')
            ->withOrganizations($this->organizationRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.organizations.create')
            ->withOrganizationTypes(OrganizationType::all()->map(function ($type) {
            return ['value' => $type->id, 'label' => $type->name];
        }));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreOrganizationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrganizationRequest $request)
    {
        $requestData = $request->only([
            'name',
            'type_id',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'phone',
        ]);
        $type = $request->input('type');

        if (!$requestData['type_id'] && $type['label']) {
            $organizationType = OrganizationType::updateOrCreate(['name' => $type['label']]);
            $requestData['type_id'] = $organizationType->id;
        }

        $organization = Organization::create($requestData);

        $request->session()->flash('flash_success', __('alerts.backend.organizations.created'));

        return $organization;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.organizations.show')
            ->withOrganization(Organization::with(['users', 'students'])->find($id))
            ->withModules(Module::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        return view('backend.organizations.edit')
            ->withOrganization($organization)
            ->withOrganizationTypes(OrganizationType::all()->map(function ($type) {
                return ['value' => $type->id, 'label' => $type->name];
            }));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateOrganizationRequest  $request
     * @param  Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOrganizationRequest $request, Organization $organization)
    {
        $requestData = $request->only([
            'name',
            'type_id',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'phone',
        ]);
        $type = $request->input('type');

        if (!$requestData['type_id'] && $type['label']) {
            $organizationType = OrganizationType::updateOrCreate(['name' => $type['label']]);
            $requestData['type_id'] = $organizationType->id;
        }
        $organization->update($requestData);

        $request->session()->flash('flash_success', __('alerts.backend.organizations.updated'));

        return $organization;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->organizationRepository->deleteById($id);

        return redirect()->route('admin.organizations.index')
            ->withFlashSuccess(__('alerts.backend.organizations.deleted'));
    }

    public function organizationTypes(Request $request)
    {
        $query = $request->get('query', null);
        if ($query) {
            $organizationTypes = OrganizationType::where('name', 'LIKE', "%${query}%")->get();
        } else {
            $organizationTypes = OrganizationType::all();
        }

        $organizationTypes->transform(function ($type) {
            return ['value' => $type->id, 'label' => $type->name];
        });

        return response()->json([
            'status' => 'success',
            'data' => $organizationTypes,
        ]);
    }
}
