<?php

namespace App\Http\Controllers\Backend;

use App\Models\Module;
use App\Models\ModuleQuestion;
use App\Repositories\Backend\ModuleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModulesController extends Controller
{
    protected $moduleRepository;

    public function __construct(ModuleRepository $moduleRepository)
    {
        $this->moduleRepository = $moduleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.modules.index')
            ->withModules($this->moduleRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $updateData = $request->only(['name', 'description', 'instructor', 'video_url']);
        $questions = $request->input('questions', []);
        $preQuestions = collect(array_get($questions,'pre', []))->pluck('question_data');
        $postQuestions = collect(array_get($questions,'post', []))->pluck('question_data');
        $module = Module::create($updateData);

        if (count($preQuestions) > 0) {
            foreach($preQuestions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['pre_or_post'] = 'pre';
                    $question['module_id'] = $module->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = ModuleQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        if (count($postQuestions) > 0) {
            foreach($postQuestions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['pre_or_post'] = 'post';
                    $question['module_id'] = $module->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = ModuleQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        $request->session()->flash('flash_success', __('alerts.backend.modules.created'));

        return $module;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('backend.modules.show')
            ->withModule(Module::with(['questions', 'students'])->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Module  $module
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        $modulePreQuestions = $module->questions()
            ->where('pre_or_post', '=', 'pre')->orderBy('question_number')->get()
            ->map(function ($question) {
                return [
                    'id' => $question->id,
                    'question_answer' => $question->question_answer,
                    'question_text' => $question->question_text,
                    'question_type' => $question->question_type,
                    'question_options' => $question->question_options,
                ];
            });

        $modulePostQuestions = $module->questions()
            ->where('pre_or_post', '=', 'post')->orderBy('question_number')->get()
            ->map(function ($question) {
                return [
                    'id' => $question->id,
                    'question_answer' => $question->question_answer,
                    'question_text' => $question->question_text,
                    'question_type' => $question->question_type,
                    'question_options' => $question->question_options,
                ];
            });
        return view('backend.modules.edit')
            ->withModule(collect([
                "id" => $module->id,
                "name" => $module->name,
                "description" => $module->description,
                "instructor" => $module->instructor,
                "video_url" => $module->video_url,
                "questions" => [
                    "pre" => $modulePreQuestions,
                    "post" => $modulePostQuestions,
                ]
            ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Module  $module
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Module $module)
    {
        $updateData = $request->only(['name', 'description', 'instructor', 'video_url']);
        $questions = $request->input('questions', []);
        $preQuestions = collect(array_get($questions,'pre', []))->pluck('question_data');
        $postQuestions = collect(array_get($questions,'post', []))->pluck('question_data');
        $saved = $module->update($updateData);

        if (count($preQuestions) > 0) {
            foreach($preQuestions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['pre_or_post'] = 'pre';
                    $question['module_id'] = $module->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = ModuleQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        if (count($postQuestions) > 0) {
            foreach($postQuestions as $index => $question) {
                if (strlen(trim($question['question_text']))) {
                    $question['id'] = isset($question['id']) ? $question['id'] : null;
                    $question['pre_or_post'] = 'post';
                    $question['module_id'] = $module->id;
                    $question['question_number'] = $index + 1;
                    $questionRecord = ModuleQuestion::updateOrCreate(['id' => $question['id']], $question);
                }
            }
        }

        $request->session()->flash('flash_success', __('alerts.backend.modules.updated'));

        return $module;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->moduleRepository->deleteById($id);

        return redirect()->route('admin.modules.index')
            ->withFlashSuccess(__('alerts.backend.modules.deleted'));
    }
}
