<?php

namespace App\Http\Controllers\Backend;

use App\Models\Student;
use App\Repositories\Backend\Auth\UserRepository;
use App\Repositories\Backend\StudentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentsController extends Controller
{
    private $studentRepository;
    private $userRepository;

    public function __construct(StudentRepository $studentRepository, UserRepository $userRepository)
    {
        $this->studentRepository = $studentRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.students.index')
            ->withStudents($this->userRepository->getActiveStudentsPaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->userRepository->where('id', $id)->first();
        return view('backend.students.show')
            ->withStudents($user->students)
            ->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('backend.students.edit')->withStudent($student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function clearProgress($id)
    {
        $user = $this->userRepository->where('id', $id)->get();

        if (!$user) {
            return redirect()->route('admin.students.index')
                ->withFlashDanger(__('alerts.backend.students.user_not_found'));
        }

        $this->studentRepository->where('user_id', $id)->get()->each(function ($student) {
            return $student->delete();
        });

        return redirect()->route('admin.students.index')
            ->withFlashSuccess(__('alerts.backend.students.progress_deleted'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
